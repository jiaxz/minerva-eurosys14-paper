\section{Deep Learning Background}	\label{sec:Background}
This section introduces some of the most basic concepts of deep learning, a branch of the general machine learning disciplines. A special category of deep neural network that is particularly effective in learn spatially correlated data (e.g. image) is called {\it convolutional neural network} (CNN). Its computation and storage patterns are different enough from a normal neural network, and deserves separate discussion.


\subsection{Model, Data and Training Algorithm}	
A machine learning task is to learn the parameters of a model from example data by a training algorithm. In deep learning, the models vary from discriminative models such as Deep Neural Networks (DNN), to generative models like Deep Boltzmann Machines (DBM)(see~\cite{bengio2013representation} for a survey). Discriminative models perform classification tasks, whereas generative models aim at generating samples close to data distribution.

These models usually have a graphical representation, where the units (or neurons) are grouped into layers and connected by weighted edges across the layers. The weights are the model parameters to learn. Applying these models to classification or sample generation induces matrix computations along with the graph structure. For example, in DNN, the unit values in a layer, often called the activation of the layer, are produced from its preceding layer by a matrix computation, $x^k = f(Wx^{k-1} + b)$. Here $x^k$ and $x^{k-1}$ are vectors and $W$ is the weight matrix consisting of all the edges between the two layers, and $b$ is a bias vector. $Wx^{k-1}$ is a matrix multiplication, whereas $f$ is a non-linear activation function that is element-wisely applied. A widely used activation function is sigmoid function $1/(1+exp(-x))$. Similarly, when performing Gibbs sampling to generate data in a DBM, the similar matrix computations follows the undirected graph structure, $x^k = f(W_1 x^{k-1} + W_2^T x^{k+1} + b)$. Here, $W_1$ and $W_2$ are the weight matrices from the adjacent two layers, respectively.

The total sample data set $D$ is usually partitioned into mini-batches $\{D_1, D_2, ...\}$. Provided that different mini-batches capture similar statistic properties, model parameter updates can occur after each mini-batch, leading to faster convergence. It should be noted that while larger mini-batch sizes improves throughput, it does not necessarily speed up convergence.

The standard Stochastic Gradient Decent (SGD) training can be expressed in the following pseudo code. The gradients of loss function on different mini-batches are used to update the global model parameters $\theta$ sequentially. 
%=======
%The total sample data set $D$ is usually partitioned into mini-batches $\{D_1, D_2, ...\}$. Provided that different mini-batches capture similar statistic properties, model parameter updates can occur after each mini-batch, leading to faster convergence. This is the usual strategy in Stochastic Gradient Decent (SGD) training, which is the focus of Minerva. In normal SGD, the gradients of loss function on different mini-batches are used to update the global model parameters $\theta$ sequentially, as is depicted in the following pseudo code.
%>>>>>>> 562d09ba6bcfb6396c0c1e28451f8c87c85daae8

\lstdefinelanguage{pseudo}
{morekeywords={for,foreach,to,in,CalculateGradient},
sensitive=false,
morecomment=[l]{//},
morecomment=[s]{/*}{*/}}
\begin{lstlisting}[language=pseudo, mathescape, fontadjust=true,  columns=flexible]
    for(epoch = 1 to N):
        foreach ($D_i$ in $\{D_1, D_2, ...\}$):
            $\frac{\partial l}{\partial \theta}$ = CalculateGradient($D_i$, $\theta$)
            $\theta = \theta - \epsilon \frac{\partial l}{\partial \theta}$
\end{lstlisting}

The sequential update nature of SGD means that improving the performance needs to take the form of parallelizing computation of the training algorithm. This is called \emph{model parallelism}. A more relaxed form that achieves better scalability is to allow separate models to be trained in parallel on different portion of data, each adopts SGD strategy to train its local model. Those models, called model replicas, asynchronously exchange their parameters through a global {\it parameter server}~\cite{dean2012distbelief}. This form of parallelism is called \emph{data parallelism}. Obviously, both forms of parallelisms can be combined.

The objective of a training algorithm is to calculate the gradients of loss function respect to the parameters, and to determine how to update the parameters with these gradients. The computation of gradients also depends on the model. Usually, it is also a matrix operation following the edges of network topology. For example, the back-propagation algorithm combines a feed-forward classification phase, whose error relative to the targets are backward propagated, $y^i = f'(x^i) \odot W^T y^{i+1}$, where the $W$ is the weight matrix between $i$'th and $(i+1)$'th layer and the $\odot$ is an element-wise vector production. If $f(x)$ is sigmoid function, then $f'(x)$ is an element-wise product $f(x)\odot(1-f(x))$.

Deep learning derives much of its power by going deep with many layers of neurons, and each layer can be quite wide. One weight matrix between a pair of 2000-neuron layers immediately produces 4 million parameters. The huge parameter searching space and non-convex loss function mean that SGD can often get stuck in local minimums. Getting around them often calls for methods based on higher-order derivative properties of the loss functions, such as Quasi-Newton and Conjugate Gradient method. These advanced optimization methods start from the gradients, and conduct a series of matrix computation to get the final parameter update. Some methods, such as L-BFGS, relies on memory of the update history.

\subsection{Convolutional Neural Network}
 
%A full CNN is usually a hybrid, with the last few layers organized much as a traditional multi-layer neural networks, which takes inputs from a stack of convolutional layers.

\begin{figure}
\centering
\includegraphics[width=0.4\textwidth]{resource/CNN_intro.pdf}
\caption{The convolution and down-sampling in CNN.}
\label{fig:CNN_intro}
\end{figure}

For tasks such as object recognition, CNN has proven to be a more effective model, with a few unique structural properties that significantly depart from the normal fully-connected networks: sparse connectivity, shared weights and down-sampling. A unit in the {\it convolutional} layer only connects to a small sub-region of its input. This sub-region is called the unit's {\it local receptive field}. The set of weights connecting into a unit is alternatively called its {\it filter}, and filters of different units share same values. This weight sharing makes sure that a feature detected at one position is similarly detected at a different place, and also drastically reduces the amount of free parameters.

Mathematically, for the $i$'th unit in layer $k$, 
\[
	x_i^k = f(\sum_{j} W_j x_{r(i,j)}^{k-1}), 
\]
where $r(i,j)$ returns the index of the $j$'th input unit in unit $i$'s receptive field. Here, $f$ is the non-linear activation function, and $W$ is the shared filter weight matrix between layer $k-1$ and $k$.

One can imagine a filter is replicated and tiled in such a way as to cover the entire layer, thus the computation can be viewed as a sparse matrix multiplication. However, it is more efficient to implement it as having the filter convolve with the inputs, producing the corresponding output called a {\it feature map}. Usually, there are many parallel and different feature maps, each is trained to detect one particular feature. All the feature maps are further optionally resized by down-sampling {\it pooling} layer. The most common one is max-pooling, where each unit selects the max values in a small region. 

The CNN that Minerva implements follows the one described in~\cite{krizhevsky2012imagenet}, in which there are 5 convolutional layers, with some pooling and normalization layers in between, before entering 3 fully-connected layers, for a total of 13 layers.

% have not explained channel yet -- is it important?


%which is a spacial convolution. For the $i$'th unit in layer $k$, $x_i^k = f(\sum_{j} W_j x_{r(i,j)}^{k-1})$, where $r(i,j)$ returns the index of the $j$'th input unit in unit $i$'s receptive field. Here, $f$ is the non-linear activation function, and $W$ is the shared filter weight matrix between layer $k-1$ and $k$. Finally, each convolutional layer are further resized by down-sampling, which is usually a max-pooling layer that only select the max values from the units in a small region. Overall, both the connections for convolution and down-sampling are sparse with a regular local structure. In practice, people leverage this specific sparse structure for efficient computation.


