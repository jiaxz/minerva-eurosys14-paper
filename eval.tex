\section{Evaluation}	\label{sec:Evaluation}
\subsection{Experiment Setup}
We select two deep learning models for detailed evaluation. \emph{Speech-net} closely models what our sibling speech research group uses to train a large-vocabulary speech recognition system. The default configuration is an 8-hidden-layer network with 1100 input neurons, a 9K softmax output layer, and hidden-layers of 2K sigmoid neurons. There are a total of 44M free parameters. The default mini-batch size is 1024 frames. We were advised to keep this topology, so the experimental variations restrict to changing the mini-batch size (default is 1024 frames), unless stated otherwise.

The second network, \emph{CNN}, is a deep convolution network with the same configuration as reported in~\cite{krizhevsky2012imagenet}. There are 5 convolutional layers, each has a convolution, max-pooling and local normalization sub-layer. The max-pooling is 4 out of 1, and the filter size vary from $11\times 11\times 3$ to $3\times 3\times 384$. This is followed by 3 fully-connected layers of 4K, 4K, and 1K sigmoid neurons. We vary the model size by changing number of feature maps, but otherwise keep the architecture fixed. The small, medium (default) and big model corresponds to 0.38/1.05/3.91 billion connections. The medium model is the current record holder of the ImageNet 1K-category classification, and has 60M free parameters. Increasing the model size would lead to better results by simply raising the power of the model. A mini-batch takes 128 images, each is broken into 3 channels (for RGB) and has $224\times 224$ pixels. 

For most of the experiments, we report system throughput in terms of mini-batch (or training data) processed per second. Most of the results correspond to loading and computing data from memory, and therefore examines Minerva's computational and scaling efficiency. We enable user-level prefetching which effectively hides all the disk I/O latency. We compare against a variety of alternatives when possible.

We test the system performance over a wide range of configurations, breaking into three distinctive scenarios: desktop productivity, scale-up and scale-out with cluster. The lowest tier runs on 4-core/8GB (memory) workstation and laptop, with Intel X3360 and i7 processors clocked at 2.83 and 2.79 GHz, respectively. The next tier runs on 16-core/128GB server, with Intel Xeon E5-2665 processor clocked at 2.40 GHz, and is optionally outfitted with NVidia GTX Titan GPU (2466 cores). Finally, we have a cluster of 10 4-core workstations connected via 1Gb/s Ethernet, or the 4 16-core servers connected with a 24Gb/s Infiniband. All results are running on Linux 2.6.32.

\subsection{Preliminary Results}
\subsubsection{Results of Desktop Productivity}
Deep learning have complex training algorithms. It is far better to develop new algorithms and/or new tricks in an environment that is focused on productivity, such as Matlab or Octave. Once the algorithm stabilizes, Migrating it to Minerva enables it to be accelerated and scaled out. As one additional incentive, the algorithm enjoys better performance in exactly the same environment as it is developed. 

For both speech-net and CNN, we implemented their Matlab versions using the popular deep-learning toolbox~\cite{deeplearntoolbox}. Their corresponding Minerva codes follow the same style, and is equally compact; our CNN is about 500 lines of code, versus about 300 lines in matlab. The Matlab code uses a convolution function, which simplifies the code but is sub-optimal, thus the comparisons on CNN is not exact. Minerva is about 15 times faster than Matlab. For speech-net with changing mini-batch size (from 256 to 4K), Minerva is 1.5 $\sim$ 2.0 times and 7.0 $\sim$ 8.6 times faster than Matlab and Octave, respectively.

The above results are from the 4-core workstation; we get qualitatively similar results on a Lenovo X1 labtop. Note that both Minerva and Matlab rely on MKL to carry out matrix computation, and MKL is already multi-threaded and leverages multi-core well. This performance gap is a justification for Minerva's design, as we will explain next.

\subsubsection{Results of Scaling-up} 
\label{sec:Scale-up}
In this category, we report our performance on high-end server, optionally with a GPU. Before presenting the results, we want to describe a valid design option that is also relevant to the performance discrepancy against Matlab. It can be argued that, since MKL is multi-thread ready and can exploit multi-core, we can directly code against MKL calls without going through the trouble of translating into an intermediate dataflow representation. The program will be equally compact, and the system is much simpler.
 
We have already presented performance results on the 4-core machine that contradict this MKL-only strategy. The gap persists to more cores, and is algorithm dependent. In Table~\ref{tab:VaryingConfig}, we ran speech-net and CNN with varying configurations in a 16-core machine. In addition, we configure the number of partitions and the number of MKL threads for matrix computation, and the product of the two is equal to total number of cores. Therefore, the MKL-only approach corresponds to the case of one single partition.

The result of CNN shows that the optimal performance is the opposite of MKL-only approach; limiting MKL thread to be one and setting number of partitions to be number of cores gives the best performance. The MKL-only strategy is 5.7x, 4.5x, and 2.8x slower, for small, medium and large models. The reason is that the convolution nature results in many small-matrix multiplications. Thus, dataflow on top of MKL exploits parallelism better, allowing many such multiplications to proceed in parallel.
 
The result of speech-net is less clear. As can be seen, the best performance is a compromise, and its configuration depends on both the model and the data. For speech-net with 4K samples per mini-batch, 8 partitions with 2-threads MKL performs nearly twice better. Minerva's dataflow engines delivers the flexibility to configure for the optimal performance. Note that although such flexibility leads to many parameters to tune, the performance is stable once a configuration is chosen.

\begin{table}
% \centering

{\it Speech-net}

\begin{tabular}{|c|c c c c c|}
\hline
Mini-batch &  \multicolumn{5}{|c|}{Partition Number/\#MKL threads} \\
size & 1/16 & 2/8 & 4/4 & 8/2 & 16/1 \\
\hline
\hline
256 & 2.85 & 1.70 & \textbf{1.48} & 2.48 & 6.58 \\
512 & 4.06 & 2.50 & \textbf{2.29} & 3.09 & 6.99 \\
1024 & 7.08 & 6.02 & 4.77 & \textbf{4.27} & 6.78 \\
2048 & 13.90 & 9.41 & 7.96 & \textbf{7.14} & 10.10 \\
4096 & 25.37 & 18.46 & \textbf{15.37} & 14.9 & 15.82 \\
8192 & 50.85 & 36.05 & 30.46 & \textbf{28.91} & 32.39 \\
\hline
\end{tabular}

~\\
~\\
{\it CNN}

\begin{tabular}{|c|c c c|}
\hline
Model &  \multicolumn{3}{|c|}{Partition Number/\#MKL threads} \\
Size & 1/16 & 4/4 & 16/1 \\
\hline
\hline
Small &	101.41 &	35.97 &	\textbf{17.82} \\
Medium & 126.78 & 50.51 & \textbf{27.58} \\
Large & 240.63 & 116.31 & \textbf{86.59} \\
\hline
\end{tabular}
\caption{Speech-net(top) and CNN(bottom) performance variation. We use different configuration by varying mini-batch size (for speech-net) and model size (for CNN). In each configuration, the partition number is varied with fixed total threads equal to number of cores. The times for each mini-batch are shown, and the optimal times for each configuration are highlighted. The first column corresponds to the MKL-only strategy.}
\label{tab:VaryingConfig}
\end{table}

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{resource/CNN_problemScale.pdf}
\caption{The time for each mini-batch in CNN training at different model size.}
\label{fig:CNN_problemScale}
\end{figure}

Figure~\ref{fig:CNN_problemScale} shows the results of CNN on a 16-core CPU and GPU, comparing against ConvNet, a popular hand-tuned GPU package for deep convolutional network. Focus first on our 16-core CPU performance, scaling the model size scales the running time almost linearly. The GPU performance, however, enjoys super-linear scalability. The running time of the medium model is only 1.3x over the small model. Similarly, the big model is 2.3x over the medium model, despite the fact that it's nearly 4 times larger. Such super-linear scalability is likely a result that GPU runs more efficiently with larger matrix.
 
Surprisingly, Minerva's GPU performance exceeds ConvNet, and sometimes by a large margin. While the two are comparable when the model is small, Minerva runs 40\% faster on the medium size. At that model size, ConvNet already used up 5.5GB (out of the 6GB) GPU memory, and cannot run the big model. 
 
These results are encouraging. Although CPU does not perform as well as GPU, its performance is decent enough, and it has the advantage to run much larger model.

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{resource/Speech_miniBatch.pdf}
\caption{The throughput of speech-net training at different mini-batch size.}
\label{fig:Speech_miniBatch}
\end{figure}

The result of speech-net with varying mini-batch size is depicted in Figure~\ref{fig:Speech_miniBatch}. The CPU performs roughly the same, achieving around 1K samples per second. Larger mini-batch improves the utilization of GPU, and with 8K mini-batch, we reach nearly 7K samples per second. This result matches well with hand-tuned GPU implementation from our sibling speech research group.

\subsubsection{Results of Scaling-out}

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{resource/ScalingOut.pdf}
\caption{The throughput speed-up with increasing machine numbers for speech-net (top) and CNN (bottom).}
\label{fig:ScalingOut}
\end{figure}

We tested model parallelism on the 4-core cluster, and enabled user-level network I/O optimization with mini-mini-batch (see Section~\ref{sec:user-opt}) . The results are shown in Figure~\ref{fig:ScalingOut}. The default speech-net is too small to take full advantages of the total number of cores available, therefore we vary the model size of speech-net while keeping the 4K mini-batch size. 

Our result is consistent with the results in DistBelief~\cite{dean2012distbelief}, in that the speech-net scales relatively poorly because the layers are all-to-all connected, whereas CNN has lower cross-machine traffic and scales better, achieving nearly 4x speedup with 8 machines. We emphasize that we parallelize a full CNN model. In contrast, the model in DistBelief is a much simpler subset, in that it uses local receptive fields and no weight sharing. 

Scaling out using data parallelism is relatively straightforward. However, we found that improved system throughput does not necessarily translate into convergence speed directly, and much tuning is needed. We will include the results of data parallelism in the final report.

% model parallelism on GPU farm

% data parallelism



