/*
 * Basic BP algorithm with one sample update
 */
#include <graphlab.hpp>
// #include <graphlab/macros_def.hpp>

/////////////////////////////////////

float ALPHA = 0.0;

enum NEURON_STATE
{
	FF,
	BP
};

// The vertex data is the layer, neuron value (activation/error delta)
// and whether in FF process
typedef struct vertex_data : graphlab::IS_POD_TYPE {
  float activation;
  float delta;
  NEURON_STATE state;
} vertex_data_type;

// The edge data is the weight
typedef float edge_data_type;

// The graph type is determined by the vertex and edge data types
typedef graphlab::distributed_graph<vertex_data_type, edge_data_type> graph_type;
 
class simpleBP :
  public graphlab::ivertex_program<graph_type, float>,
  public graphlab::IS_POD_TYPE  {
public:
   
  int flip_state(int state) {
	switch(state) {
		case BIAS: return BIAS;
		case FF: return BP;
		case BP: return FF;
	};
	return -1;
  }

  /* The gather edges depend on whether the neuron is perform FF or BP */
  edge_dir_type gather_edges(icontext_type& context, const vertex_type& vertex) const {
	switch(vertex.data().state)
	{
	case FF: return graphlab::IN_EDGES;
	case BP: return graphlab::OUT_EDGES;
	};
  }

  /* Gather the weighted sum of neuron activation */
  float gather(icontext_type& context, const vertex_type& vertex, edge_type& edge) const {
	switch(vertex.data().state)
	{
	case FF:
		float lower_activation = edge.source().data().activation;
		float weight = edge.data();
		return weight * lower_activation;
	case BP:
		float higher_delta = edge.target().data().delta;
		float weight = edge.data();
		float self_activation = vertex.data().activation;
		// Learning the weight
		edge.data() = weight + ALPHA * self_activation * higher_delta;
		return weight * higher_delta;
	};
  }

  /* Use the total rank of adjacent pages to update this page */
  void apply(icontext_type& context, vertex_type& vertex, const gather_type& total) {
	switch(vertex.data().state)
	{
	case FF:
		vertex.data().activation = sigmoid(total);
		break;
	case BP:
		float activation = vertex.data().activation;
		float delta = total * activation * (1 - activation);
		vertex.data().delta = delta;
	};
	vertex.data().state = flip_state( vertex.data().state ); // flip FF/BP state TODO flip in apply function SUCKS!
  }

  /* The scatter edges depend on whether the pagerank has converged */
  edge_dir_type scatter_edges(icontext_type& context, const vertex_type& vertex) const {
	switch(vertex.data().state)// TODO attention: have fliped in apply function
	{
	case BP: return graphlab::OUT_EDGES;
	case FF: return graphlab::IN_EDGES;
	};
  }

  /* The scatter function just signal adjacent pages */
  void scatter(icontext_type& context, const vertex_type& vertex, edge_type& edge) const {
	vertex_type other = vertex.id() == edge.source().id()? edge.target() : edge.source();
	context.signal(other);
  }
	
  float sigmoid(float x) {
	return 1.0 / (1.0 + exp(-x));
  }
  
}; // end of simple_bp update functor

