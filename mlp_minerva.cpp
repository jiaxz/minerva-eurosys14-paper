M_FLOAT Sigmoid(M_FLOAT x)
{
	return 1.0 / (1.0 + exp(-x));
}	

void tests(int argc, char** argv)
{
	MinervaSystem ms(argc, argv);
	// Building graph model
	Model model(ms);
	Layer layer0 = model.AddLayer(500);
	Layer layer1 = model.AddLayer(1000);
	Layer layer2 = model.AddLayer(500);
	model.AddConnection(layer0, layer1);
	model.AddConnection(layer1, layer2);
	model.Finalize();

	int numEpochs = 10;
	size_t numSamplesPerMB = 128;

	// Root matrices
	Matrix w01 = Matrix(ms, layer1, layer0, 0.1);
	Matrix w12 = Matrix(ms, layer2, layer1, 0.1);
	std::vector<Matrix> inputs = LoadBatches(ms, layer0, _, numSamplesPerMB, "input.dat");
	std::vector<Matrix> labels = LoadBatches(ms, layer2, _, numSamplesPerMB, "label.dat");

	// Learning Procedure
	size_t numMiniBatches = inputs.size();
	for(int epoch = 0; epoch < numEpochs; ++epoch)
		for(size_t i = 0; i < numMiniBatches; ++i)
		{
			// FF
			Matrix y1 = (w01 * inputs[i]).Map(&Sigmoid);
			// y1.Eval(); // uncomment this line to evaluate y1. 
			Matrix y2 = (w12 * y1).Map(&Sigmoid);
			// BP
			Matrix err = y2 - labels[i];
			Matrix d2 = err.Dot(y2).Dot(1 - y2);
			Matrix g12 = d2 * y1.Transpose() / numSamplesPerMB;
			Matrix d1 = (w12.Transpose() * d2).Dot(y1).Dot(1 - y1);
			Matrix g01 = d1 * inputs[i].Transpose() / numSamplesPerMB;
			// update
			w01 += g01;
			w12 += g12;
		}
	w01.Eval();
	w12.Eval();
}

