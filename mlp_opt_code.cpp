//////////////////////// mini mini batch
	size_t numSamplesPerMMB = 128 / 4;
	vector<Matrix> inputs = LoadBatches(ms, layer0, _, numSamplesPerMMB, "input.dat");
	vector<Matrix> labels = LoadBatches(ms, layer2, _, numSamplesPerMMB, "label.dat");
	size_t numMMB = inputs.size();
	for(int epoch = 0; epoch < numEpochs; ++epoch)
		for(size_t i = 0; i < numMiniBatches; ++i)
		{
			vector<Matrix> g01mmb(4);
			vector<Matrix> g02mmb(4);
			for(size_t j = 0; j < 4; ++j)
			{
				size_t mmbIdx = i * 4 + j;
				... // inputs[mmbIdx], labels[mmbIdx]
				g01mmb[j] = ...;
			}
			w01 += MatVSum(g01mmb);
			w02 += MatVSum(g02mmb);
		}
//////////////////////// mini mini batch

//////////////////////// prefetch data batches
	vector<Matrix> inputs, labels, nextinputs, nextlabels;
	vector<string> inputfiles, labelfiles;
	inputs = LoadBatches(ms, layer0, _, numSamplesPerMB, inputfiles[0]);
	labels = LoadBatches(ms, layer2, _, numSamplesPerMB, labelfiles[0]);
	for(int epoch = 0; epoch < numEpochs; ++epoch)
		for(size_t batchIdx = 1; batchIdx < numBatchFiles; ++batchIdx)
		{
			nextinputs = LoadBatches(ms, layer0, _, numSamplesPerMB, inputfiles[batchIdx]);
			nextlabels = LoadBatches(ms, layer0, _, numSamplesPerMB, labelfiles[batchIdx]);
			for(size_t i = 0; i < numMiniBatches; ++i)
			{
				...
			}
			w01.Eval();
			w02.Eval();
			inputs = nextinputs;
			labels = nextlabels;
		}
//////////////////////// prefetch data batches	