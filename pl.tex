\section{Programming Model}	\label{sec:ProgrammingModel}

%------------- The common configuration for all C++ code ---------------
\lstset{language=C++,
tabsize=2,
basicstyle=\ttfamily\footnotesize,
columns=flexible, keepspaces=true, basewidth={0.3em, 0.3em}, fontadjust=true,
breaklines=false,
keywordstyle=\color{kwcolor}\bfseries,
stringstyle=\color{constcolor},
morekeywords={include}
}

\subsection{A detour of using graph API}

\begin{figure}

\centering
\includegraphics[width=0.20\textwidth]{resource/GraphProgram.pdf}

\lstset{
emph={[1]Feedforward,Backprop}, emphstyle=[1]\color{constcolor},
emph={[2]HiddenLayer,InputLayer,OutputLayer,JumboVertex,Edge,Matrix,Direction}, emphstyle=[2]\color{typecolor},
emph={[3]Init,Upper,Lower,LowerEdge,UpperEdge,VertexProgram, ElementWiseProduct,Transpose,Trigger}, emphstyle=[3]\color{funccolor},
commentstyle=\color{cmtcolor}
}

%  JumboVertex & Upper() {...} // Get the upper Vertex
%  JumboVertex & Lower() {...} // Get the lower Vertex
%  Edge & UpperEdge() {...}    // Get the upper edge
%  Edge & LowerEdge() {...}    // Get the lower edge

\begin{lstlisting}
class HiddenLayer : public JumboVertex {
  enum Direction {Feedforward, Backprop} status;
public:
  Matrix act, error;    //Define activation and error
  Matrix bias;

  void Init() {
  	status = Feedforward; ...
  }
  void VertexProgram() {
    if (status == Feedforward) {
      act = LowerEdge().W * Lower().act + bias;
      status = Backprop; Trigger(Upper());
    }
    else {
      error = ElementWiseProduct(act, 1-act,
        UpperEdge().W.Transpose() * Upper().error);
      status = Feedforward; Trigger(Lower());
    } ...
  }
};
class  InputLayer : public JumboVertex { ... };
class OutputLayer : public JumboVertex { ... };
\end{lstlisting}

\caption{Graph program of training a 3-layer feed-forward neural network. Only the snippet of the hidden layer vertex program is shown.}
\label{fig:GraphProgram}
\end{figure}

Given that neural networks take the form of a graph, it's tempting to implement a deep-learning algorithm using graph programming model. Indeed, our first prototype of the programing model leverages the gather/scatter/apply API in GraphLab~\cite{low2010graphlab}. For each neuron type, we can define its vertex program that interacts with other neurons. One immediate benefit of this approach is that the programming model directly expresses all parallelism inherent in the algorithm.

However, we immediately ran into a number of issues. First, all neurons in a layer share the same connectivity pattern; it is entirely unnecessary and wasteful to specify connectivity per neuron. Second, the full connection between two layers and the multiplicative weights on every edge implies a more efficient implementation using matrix multiplication. These two issues can be solved by specifying a ``jumbo'' vertex, which represents an entire layer of neurons and employs the matrix computation. A prototype of such a vertex is shown in Figure~\ref{fig:GraphProgram}.

However, the logics of the vertex program depends on whether it is triggered from below or from top. In this example, in the bottom-up direction we need to compute activation, and in the top-down direction we need to compute the error signals. The graph program now starts to feel clumsy.

What finally made us to give up the graph computing paradigm is that the entire training algorithm is now broken into isolated vertex programs. Such fragmentation leads to tedious inference of the overall program flow, and debugging becomes a nightmare. This entirely defeats the purpose of being user friendly.

\subsection {Minerva Matrix API}


\lstset{
emph={[1]FULL, RANDOM, SUM}, emphstyle=[1]\color{constcolor},
emph={[2]MinervaSystem,Model,Layer,Matrix,ParameterSet}, emphstyle=[2]\color{typecolor},
emph={[3]Sigmoid, MLP_Training, AddLayer, AddConnection, Finalize, LoadBatches, Map, Transpose, Agg, AggPerRow, AggPerCol, EvalAll, EleWiseProd,op, SetPartition, AddConvConnect, exp, Add, RegisterToParameterServer, PushToParameterSever, PullFromParameterSever}, emphstyle=[3]\color{funccolor},
commentstyle=\color{cmtcolor}
}


% \begin{lstlisting}  void AddConnection(layer1, layer2, conn)  \end{lstlisting}

\begin{table}
\begin{tabular}{|c|l|}
\hline
Category & APIs\\
\hline\hline
Building Model &
\begin{lstlisting}
Layer layer = AddLayer(dim)
AddConnection(layer1, layer2, type)
AddConvConnect(layer1, layer2,...)
\end{lstlisting} \\
\hline
Creating Matrix &
\begin{lstlisting}
A = Matrix(layer1, layer2, init)
A = Matrix(layer, dim, init)
A = Matrix(dim, layer, init)
vector<Matrix> LoadBatches(layer,
          dim, batch_size, filename)
\end{lstlisting} \\
\hline\hline
\multiRow{Element-wise OP \\ (unary)} &
\begin{lstlisting}
B = A.Map(func)
B = A op val
B = val op A
\end{lstlisting} \\
\hline
\multiRow{Element-wise OP \\ (multivariate)} &
\begin{lstlisting}
C = A op B
C = EleWiseProd(A1, A2, ...)
\end{lstlisting} \\
\hline
\multiRow{Matrix \\ Multiplication}  &
\begin{lstlisting}
C = A * B
\end{lstlisting} \\
\hline
Aggregation &
\begin{lstlisting}
B = A.Agg(agg_type)
B = A.AggPerRow(agg_type)
B = A.AggPerCol(agg_type)
\end{lstlisting} \\
\hline
Matrix Layout &
\begin{lstlisting}
B = A.Transpose()
\end{lstlisting} \\
\hline\hline
Model Paralelism &
\begin{lstlisting}
SetPartition(layer, part_plan)
\end{lstlisting} \\
\hline
Data Paralelism &
\begin{lstlisting}
RegisterToParameterServer(para_set)
PushToParameterSever(para_set)
PullFromParameterSever(para_set)
\end{lstlisting} \\
\hline
\end{tabular}
\caption{The Minerva API. {\tt A}, {\tt B} and {\tt C} are all matrices. {\tt layer} is a {\tt Layer} type object. Other variables are self-explainable. {\tt op} stands for system-defined operator like $+$ ,$-$ ,$\times$, etc. Refer to example for their usage.}
\label{tab:ApiTable}
\end{table}

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{code_MLP_cropped.pdf}

%\lstinputlisting[numbers=left, numbersep=0.2em, %numberstyle=\scriptsize]{resource/MinervaProgram_MLP.cpp}

\caption{The program to train a three-layer neural network in Minerva.}
\label{fig:MinervaProgram}
\end{figure}

Even though we gave up the graph APIs, the prototyping experience is nonetheless beneficial. For example, we learned that describing the model topology with graph is both intuitive and straightforward. More importantly, it reveals the insights that the training programs are a series of matrix operations that are induced from the graphical representation of the model. Thus, Minerva defines a set of APIs (Table~\ref{tab:ApiTable}) to cover three distinctive stages of a deep-learning algorithm: describing the high-level neural net architecture, declaring the primary matrices, and finally, specifying the learning procedure. The programming style is imperative and procedure, much like how one develops algorithm in Matlab. Figure~\ref{fig:MinervaProgram} shows the code snippet of a simple 3-layer feed-forward neural network.

A Minerva \emph{model} defines two entities: \emph{layer} and \emph{connection}. User declares a layer by calling {\tt model.AddLayer(dim)}, and {\tt model.AddConnection(l1, l2)} to connect the two layers. For the network of this example, {\tt dim} is a scalar that specifies number of neurons, and the type of connection is all-to-all. Line 9-16 builds the model in Figure~\ref{fig:MinervaProgram} (we will explain partitioning shortly). So far, we have found that these two simple APIs are sufficient to describe all the models that we have studied.

The basic data type in deep learning algorithm is matrix. For example, the hidden layer's activation is computed with $y = sigmoid(Wx + b)$ (line 40). To carry out this statement, matrix $W$, $x$ and $b$ must be declared. In Minerva, matrices are either directly derived from the model (e.g. {\tt W}, {\tt b} and {\tt inputs}), or are results of computation (e.g. {\tt y}). In either case, they are attached to the model, via either explicit declaration or program inference. In this example, {\tt x} is attached to {\tt layer1}, {\tt y} and {\tt b} are attached to {\tt layer2}, and {\tt W} is attached to the connection in between (see Figure~\ref{fig:GraphPartiton}(a)). This is important for Minerva to automatically infer partitioning scheme and make placement decision, explained shortly.

%The {\it root} matrices are \emph{derived} from the model. For example, line 22-25 derives the weight matrices and the bias vectors. Note that {\tt x} is also a root matrix, which points to input (i.e. a minibatch of training samples) and is loaded by {\tt LoadBatches} (line 26). Non-root matrices such as {\tt y}, {\tt z} are outputs of computation and can be freely declared on the fly, with their dimension automatically inferred (line 40, 41). The distinction of root and non-root matrices are internal to Minerva, the only constraint the user should care is to derive the matrices from the model. 

Writing training procedure is straightforward. Line 38-49 corresponds to the feed-forward pass,  back propagation and the weight update. The style of the program is similar to many of the Matlab implementation of deep learning algorithms we have studied. We incrementally add operators as we study and implement various deep learning networks. Table~\ref{tab:ApiTable} summarizes the major ones. The most important types are \emph{element-wise} operator and an extended version of \emph{multiplication} operator. In addition, there are various forms of aggregation operations for error calculation or normalization.

CNN requires some special treatments. Recall that each layer in CNN is a stack of feature maps, and each of which is a 2-D or 3-D data. Thus, the input and output of each layer actually has a internal multi-dimensional structure. Furthermore, instead of a full connection, a filter is replicated and tiled between the two layers. All these structural information are given by a single interface {\tt AddConvConnect(layer1, layer2,...)}. Then, the {\tt Matrix} class operators such as {\tt +} and {\tt *} automatically takes appropriate implementation (see Section \ref{sec:convolution_design}).

\subsection{Expressing parallelism}

\begin{figure}
\centering
\includegraphics[width=0.4\textwidth]{resource/GraphPartition.pdf}
\caption{Partitioning on the bottom first layers in Figure~\ref{fig:MinervaProgram} and their attached matrices ({\tt x, W, y, b}). The result partitions (right) are allocated to two machines (gray and white).}
\label{fig:GraphPartiton}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{resource/DataFlow.pdf}
\caption{The mathematic formula, its corresponding Minerva statement and the dataflow graph for a one step feed-forward. Splitting data yields partitioned dataflow graph that expresses more parallelism; the data and computation vertices with different colors reside and are executed on different processes/machines.}
\label{fig:DataFlow}
\end{figure}

Minerva runtime takes the user code and executes symbolically until hitting an {\tt EvalAll()} statement (line 54), building a dataflow graph that is evaluated concretely. The dataflow fragment that corresponds to the feed-forward computation (line 40) is depicted in Figure~\ref{fig:DataFlow}(a). This dataflow representation expresses all the potential parallelism inherent to the training algorithm. To improve performance further, user can elect to employ model parallelism (line 13-14), data parallelism (line 30-33, 52-53), or both of them. These options are entirely optional, and the system runs correctly without them.
%The dataflow is also partitioned and distributed to multiple machines based on the previous model and matrix partitioning. 
%provide a simple interface to let users specify their desired {\it model parallelism} and {\it data parallelism}.

\textbf{Model parallelism}
The first is to parallelize the computation by partitioning the data. But instead of doing that on the data directly, this is done by partitioning the model layers, using the {\tt SetPartition(layer, partplan)} interface. This may seem unnatural at the beginning, but it avoids the clumsiness of having to do it for \emph{all} model-wise matrices. There are 11 total matrices in this simple example, and many more in a more complex network. Partitioning all of them correctly is tedious at best. 

Line 15 instructs that {\tt layer1} and {\tt layer2} be both partitioned into two equal portion, which splits the original model (Figure~\ref{fig:GraphPartiton}). As the result, all the matrices that attach to them are partitioned, and this is done via program inference automatically. The split results in their partitioning scheme in Figure~\ref{fig:GraphPartiton}(b). After partitioning, the equivalent dataflow graph (Figure~\ref{fig:DataFlow}(b)) now expresses more parallelism.

%Note that matrices attached to connection (such as {\tt W}).
%  {\tt W}, {\tt y} and {\tt b} are attached to these layers and connection
%
% By definition and inference from the program, Minerva knows matrix .
%
%   As illustrated in Figure~\ref{fig:GraphPartiton}, the layer 1 and layer 2 in  are partitioned into two components by running line 15 in both Figure~\ref{fig:MinervaProgram}. The connection between the two layers is correspondingly partitioned into four components.  Then, these matrices are partitioned into blocks aligned with these model components. Furthermore, Minerva determines the location of the these partitioned model components. It keeps the rule that a connection component should be co-located with either one of its neighbor layer components. Correspondingly, any matrix block is located to the machine hosting its attaching model component.
%
% calling {\tt SetPartition(layer, partplan)}.
%Our runtime executes the user code symbolically, building a dataflow graph that is evaluated concretely after hitting an {\tt eval()} statement (line 54). This process iterates until the end of the training. The dataflow fragment that corresponds to a layer of feed-forward computation (line 40, 41) is depicted in Figure~\ref{fig:DataFlow}. The dataflow representation expresses all the potential parallelism inherent to the training algorithm.
%
%\textbf{Model parallelism}~The user can further partition the matrices to extract more parallelism. The partitioning needs only be done at the layer, by calling {\tt SetPartition(layer, partplan)}. A typically usage is to partition the layer in equal portion. If {\tt layer1} and {\tt layer2} are both partitioned 2 times, then the weight matrix connecting these two layers is carved up into a 2x2 grid. Parsing the partitioning statements (line 15) allows the runtime to transform the logical dataflow graph into a partitioned dataflow graph, and naturally exposes more parallelism (see Figure~\ref{fig:DataFlow}). Note that typically all the matrices are global and need to be partitioned. A salient feature of Minerva's design is that the partitioning schemes of most of them are automatically inferred. In this example, we only set the partitioning of the two layers.

\textbf{Data parallelism}~In addition, the system can ran train multiple model replicas in parallel. The replicas execute the same user code but on different set of machines, and synchronize through a logically centralized parameter server. To enable data parallelism, we first register to the parameter server the set of parameters to be synchronized (line 30-33). User can control the pace to update and refresh its parameter set (line 52-53). The update sends deltas, whereas the refresh downloads the entire new set. Since weight updates are communicative, the logic in parameter server is simple.

%To leverage {\it data parallelism}, multiple replicas of this minerva code are ran on different sets of machines, each for a separate model training. These models trained in parallel need share their parameters in time with acceptable network cost. Since only the users know their parameters and the sharing frequency, we provide a set of APIs to let users control their data parallelism. In line 30-33, users can register their parameters on the central parameter server by explicitly assigning the names. The parameters with the same name from different models share the same value in the parameter server. In line 52-53, users decide the moment to push/pull parameters to/from the parameter server.

% Although declaring partitioning for convolution network takes the same API, the parallelism is exploited in a different way. Instead of carving up the filters, the input is divided into sub-domain, and the filters convolve in each of them.

