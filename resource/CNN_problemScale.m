% function main()

size = [
0.38
1.05
3.91
];

MinCPU = [
1.23
2.24
8.83
];

MinGPU = [
0.63
0.81
1.84   
];

Convnet = [
0.7
1.4
];


figure(1);
clf;
hold on;
SetFigureSize(450,200);

plot(size, MinCPU, 'k-o');
plot(size, MinGPU, 'k-s');
plot(size(1:2), Convnet(1:2), 'k-^');

xTickLabel = {};
for i = 1:length(size)
    xTickLabel{i} = sprintf('%.2f', size(i));
end

set(gca,'XTickMode','manual', ...
        'XTick',size, ...
        'XTickLabel', xTickLabel);

xlabel('Connection Number (Billion)');
ylabel('Time (second) per Mini-batch');
legend('Minerva (CPU)', 'Minerva (GPU)', 'Convnet', 'Location', 'Best');

SaveFigure(gcf, 'CNN_problemScale.pdf');