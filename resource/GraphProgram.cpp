class HiddenLayer : public JumboVertex {
  enum Direction {Feedforward, Backpropagation} status;
public:
  Matrix act, error;    //Define activation and error
  Matrix bias;

  void Init() {
  	status = Feedforward; ...
  }
  JumboVertex & Upper() {...} // Get the upper Vertex
  JumboVertex & Lower() {...} // Get the lower Vertex
  Edge & UpperEdge() {...}    // Get the upper edge
  Edge & LowerEdge() {...}    // Get the lower edge

  void VertexProgram() {
    if (status == Feedforward) {
      act = LowerEdge().W * Lower().act + bias;
      status = Backpropagation;
      Trigger(Upper());
    }
    else {
      error = ElementWiseProduct(act, 1-act,
        UpperEdge().W.Transpose() * Upper().error);
      Trigger(Lower());
    } ...
  }
};