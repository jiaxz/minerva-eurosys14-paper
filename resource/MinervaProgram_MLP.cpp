#include "minerva.h"

float Sigmoid(float x) {
	return 1.0 / (1.0 + exp(-x));
}	

void MLP_Training() {
	// Building Graph Model
	Model model;
	Layer layer1 = model.AddLayer(500);
	Layer layer2 = model.AddLayer(1000);
	Layer layer3 = model.AddLayer(10);
	model.AddConnection(layer1, layer2, FULL);
	model.AddConnection(layer2, layer3, FULL);
	SetPartition(layer1, 2); SetPartition(layer2, 2);
	model.Finalize();

	// Setting Hyper-parameters
	float rate=0.1; int numEpochs=66, batchSize=100;

	// Initializing matrices
	Matrix W = Matrix(layer2, layer1, RANDOM);
	Matrix V = Matrix(layer3, layer2, RANDOM);
	Matrix b(layer2, 1, RANDOM);
	Matrix c(layer3, 1, RANDOM);
	vector<Matrix> inputs = LoadBatches(layer1,...);
	vector<Matrix> labels = LoadBatches(layer3,...);

	// Data Parallelism: Registering Global Parameters
	ParameterSet pset;
	pset.Add("W", W);  pset.Add("V", V);
	pset.Add("b", b);  pset.Add("c", c);
	RegisterToParameterServer(pset);

	// Learning Procedure
	for(int epoch = 1; epoch <= numEpochs; ++epoch) {
		for(int i = 1; i <= inputs.size(); ++i)	{
			Matrix x = inputs[i];
			// Feedforward
			Matrix y = (W * x + b).Map(&Sigmoid);
			Matrix z = (V * y + c).Map(&Sigmoid);
			// Backpropgation
			Matrix ze = EleWiseProd(z, 1-z, z-labels[i]);
			Matrix ye = EleWiseProd(y, 1-y, W.Transpose()*ze);
			// Updating Weight
			W -= rate * ye * x.Transpose() / batchSize;
			V -= rate * ze * y.Transpose() / batchSize;
			b -= rate * ye.AggPerRow(SUM);
			c -= rate * ze.AggPerRow(SUM);
		}
		// Data Parallelism: Sync Parameters
		if (epoch % 3 == 0) PushToParameterSever(pset);
		if (epoch % 6 == 0) PullFromParameterSever(pset);
		EvalAll();
	}
}