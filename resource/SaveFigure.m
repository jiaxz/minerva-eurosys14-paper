function SaveFigure(h, filename)
    pos = get(gcf, 'PaperPosition');
    set(gcf, 'PaperSize', [pos(3), pos(4)]);
    set(gcf, 'PaperPositionMode', 'auto');
    saveas(h, filename);
end