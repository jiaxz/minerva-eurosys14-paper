% function main()

size = [
1024,
4096,
8192
];

figure(1);
clf;
hold on;
SetFigureSize(450,450);


%%%%%%%%%%%%%%%%% speech-net %%%%%%%%%%%%%%%%%%%%%%
subplot(2,1,1);
hold on;

data = [
1.28581363	1.147734327	0.868890977
1.582746938	1.898999375	1.552402863
1.780729547	2.717455988	2.837778353
];
data = [ones(3,1), data];

x = [1,2,4,8];
plot(1:4, data(3,:), 'k-o');
plot(1:4, data(2,:), 'k-s');
plot(1:4, data(1,:), 'k-^');

set(gca,'XTickMode','manual', ...
        'XTick',1:4, ...
        'XTickLabel', {'1','2','4','8'});

xlabel('Machine Number');
ylabel('Speed-up (speech-net)');
legend('Big Model', 'Medium Model', 'Small Model', 'Location', 'Best');

axis([0.7,4.2,0.5,3.5]);


%%%%%%%%%%%%%%%%%%% CNN %%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,1,2);
hold on;

data = [
1.612641084	2.277093073	2.71703854
1.596416382	2.400427655	2.537140274
1.721448109	2.866466617	3.19252805
];
data = [ones(3,1), data];

x = [1,2,4,8];
plot(1:4, data(3,:), 'k-o');
plot(1:4, data(2,:), 'k-s');
plot(1:4, data(1,:), 'k-^');

set(gca,'XTickMode','manual', ...
        'XTick',1:4, ...
        'XTickLabel', {'1','2','4','8'});

xlabel('Machine Number');
ylabel('Speed-up (CNN)');
legend('Big Model', 'Medium Model', 'Small Model', 'Location', 'Best');

axis([0.7,4.2,0.5,3.5]);

SaveFigure(gcf, 'ScalingOut.pdf');