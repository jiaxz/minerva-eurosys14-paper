function SetFigureSize(w, h)
    scrsz =  get(0,'ScreenSize');
    pos = get(gcf, 'Position');
    set(gcf, 'Position', [pos(1), pos(2), w, h]);
end