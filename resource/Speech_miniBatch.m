% function main()

size = [
1024,
4096,
8192
];

MinCPU = [
957
1187
904
];

MinGPU = [
4995
6787
7213 
];




figure(1);
clf;
hold on;
SetFigureSize(450,200);

width = 0.8;
x = (0:2)*3 + 1;
bar(x, MinCPU, 0.9*(1/3), 'FaceColor', [0.2, 0.2, 0.2]);

x = (0:2)*3 + 2;
bar(x, MinGPU, 0.9*(1/3), 'FaceColor', [0.8, 0.8, 0.8]);

for i = 1:length(MinCPU)
    text((i-1)*3+1, MinCPU(i) + 500, sprintf('%d', MinCPU(i)), 'HorizontalAlignment', 'center');
end
for i = 1:length(MinGPU)
    text((i-1)*3+2, MinGPU(i) + 500, sprintf('%d', MinGPU(i)), 'HorizontalAlignment', 'center');
end

xTickLabel = {};
for i = 1:length(size)
    xTickLabel{i} = sprintf('%d', size(i));
end

set(gca,'TickLength', [0, 0]);
set(gca,'XTickMode','manual', ...
        'XTick',(0:2)*3 + 1.5, ...
        'XTickLabel', xTickLabel);

xlabel('Mini-batch Size');
ylabel('Samples / Second');
% legend('Minerva-CPU', 'Minerva-GPU', 'Location', 'Best');
legend('Minerva-CPU', 'Minerva-GPU', 2);
axis([0, 9, 0, 10000]);

SaveFigure(gcf, 'Speech_miniBatch.pdf');