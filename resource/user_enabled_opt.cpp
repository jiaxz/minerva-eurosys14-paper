//////////////////////// mini mini batch
for(int i = 0; i < numMiniBatches; ++i) {
	for(int j = 0; j < numMiniMiniBatches; ++j) {
		// calculate gradient on the j'th mini-mini-batch
		Matrix gradient = ...;	
		parameters += gradient;
	}
	parameters.Eval();
}
//////////////////////// mini mini batch

//////////////////////// prefetch data batches
thisBatch = LoadBatches(..., inputfiles[1]);
for(int n = 1; n < numBatchFiles; ++n) {
	nextBatch = LoadBatches(..., inputfiles[ n+1 ]);
	... // Training on thisBatch
	parameters.Eval();
	thisBatch = nextBatch;
}
//////////////////////// prefetch data batches	