\section{System design}	\label{sec:Design}
Minerva supports both data and model parallelism. As in DistBelief~\cite{dean2012distbelief}, the system is logically partitioned into two tiers. There is one or more {\it model replica}, each trains a complete model. To support \emph{model parallelism}, each model replica can run on one multi-core server, a server attached GPU, or a cluster of them. Model replicas work on different partition of data sets to implement \emph{data parallelism}; they send parameter updates to the {\it parameter server} and download the updated model asynchronously (Figure~\ref{fig:Architecture}).

% add zookeeper stuff once it's built
To make our design simple, we do not make model replica fault-tolerant. Instead, parameter server safeguards the up-to-date model by storing it in an off-the-shelf distributed and persistent key-value store. Any failure in any computing entity in a model replica renders that entire replica unavailable. Upon rebooting, the recovered model replica contacts the parameter server to download the model and continues. This design is justified because model replica establishes a coarse failure domain, and typical scenario does not require a very large model replica. The cluster management is coordinated with  Zookeeper~\cite{hunt2010zookeeper}, and is currently under development. 

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{resource/Architecture.pdf}
\caption{The overall architecture of a Minerva system. It supports both data and model parallelism; each process alternates between symbolic execution to unfold the user code into a dataflow representation, and the evaluation of the dataflow. The dataflow graph is identical to all processes. Each process computes part of the graph, and communication calls are automatically inserted on the edges that cross process boundaries.}
\label{fig:Architecture}
\end{figure}

The design of model parallelism is the most complex, and is the focus of remaining of this section. Within a model replica, processes adopt a SIMD-like approach to exploit parallelism by working on different pieces of data under a shared dataflow representation of the computation. The distributed dataflow framework effectively hides communication latency. 

In each process, the entire execution is composed of phases. Each phase begins with single-threaded symbolic execution of the user code that dynamically translate a portion of the user code into the corresponding dataflow representation, and is followed by a multi-threaded evaluation of the dataflow, possibly on GPU. This arrangement naturally splits the design into frontend and backend (Figure~\ref{fig:Architecture}). Extracting performance of a distributed system, however, requires careful placement of data. We will discuss these points in this section. We will also devote separate discussion on how to support convolutional neural network well, as it is an important sub-category for object recognition. Finally, we show that, with only a few lines of user-level code change, we can add optimizations that are otherwise tedious to get.

\subsection{Frontend and dataflow representation}
The frontend starts with a \emph{program thread}, which first steps through the model-building part of the user code (e.g. line 9-16 of Figure~\ref{fig:MinervaProgram}) and  builds an internal graph representation of the model inside its \emph{modelManager}. It then processes the matrix derivation statements and the subsequent training procedure to progressively build an internal dataflow representation of the execution steps inside its \emph{flowManager}. Partition schemes of matrices are automatically inferred and checked as a side effect. The nodes in the dataflow graph are either \emph{computing node} or \emph{data node}. Computing nodes are virtual pointers that carries out the specific operation (see Table~\ref{tab:ApiTable}). These pointers implement polymorphism so that different library and engines can be called when the backend executes.

Note that all processes execute the same user code symbolically and thus will end up with an identical dataflow representation. What remains to be done is to divide the execution among the processes. This division is driven by locality, i.e. the whereabouts of the data nodes (explained shortly). The ownership of a computation node is decided by a few simple and deterministic rules, with the shared spirit that minimizes traffic to fetch data. For any data that does travel across process boundary, we insert pointers to remote send and receive calls on the edge at the dataflow graph of the sending and receiving process, respectively.

The above discussion does not touch upon how data is placed, which is probably the most important factor affecting performance when running in a distributed setting. The key trade-off is between avoiding communication and achieving load balance. A straightforward approach that produces perfect load balance is random hashing. Each data node can be assigned a unique id, and a process picks the data nodes by modular their ids over its process id. Unfortunately, this placement policy leads to a great deal of network traffic.

Our insight is that, after partitioning, matrix multiplication will entail data shuffling no matter how smart a placement policy is. Instead, we focus on element-wise operations, which are numerous in a deep learning algorithm. In the speech model we implemented, nearly 80\% of the operations are element-wise. They have low computation-to-communication ratio, but can be perfectly load balanced without incurring any network traffic, if their operands share identical partitioning and placement. Consider the activation function $y = sigmoid(W * x + b)$ (line 40 in Figure~\ref{fig:MinervaProgram}), the multiplication is followed by one addition and one sigmoid function, both of which are element-wise operations (Figure~\ref{fig:DataFlow}). Deriving matrices from model topology makes placement inference easy and automatic, as all matrices are attached to model (either to layers or to the connections).
% All the root matrices derived from a partitioned layer share the same partitioning and placement scheme (Figure~\ref{fig:MinervaProgram}). 
Note that inference is propagated to other matrices as the symbolic execution progresses. For example, if {\tt A = elem\_op(B)}, then matrix {\tt A} inherits the same placement as {\tt B}. For matrices that are attached to connection between a pair of layer (e.g. the  weight matrices), we choose to place them at either layer but deterministically (see Figure~\ref{fig:DataFlow} for example). Because the distributed dataflow execution is already capable of hiding communication, we have found this policy works well in practice.

Obviously, the inference rule is not complete. For instance, if {\tt A = B + C} and {\tt A} and {\tt B} are attached to different parts of the model, they are unlikely to share the same partitioning and placement scheme. For the time being, we check for such violations and abort. We have not found this to be a problem in practice. If this needs to be supported, we will add transparent data copy. 
 
In our implementation, the above sequence is broken into two phases. The logical and unpartitioned dataflow graph is built incrementally as the program thread symbolically executes. When an {\tt EvalAll()} statement is hit, the partitioned dataflow graph is then built. After that, the program thread pauses and the control transitions to backend with the dataflow graph.

Note that {\tt EvalAll()} can be invoked at arbitrary point of the program. For example, if {\tt EvalAll()} is called at the end of a training epoch, then a dataflow graph of an entire epoch is dispatched to the backend. Alternatively, it can be invoked after each statement, making debugging easier.

\subsection{Backend and Polymorphism}

By definition, the sources of the dataflow graph are ready when backend takes over the control. At the very beginning, they are the data nodes that will load training data from disk. The evaluation of a node generates results that progressively trigger the evaluations of its downstream nodes. All ready nodes are queued up and processed by a thread pool. When they are all processed, the control returns to the program thread for the next phase of symbolic execution. The implementation of the thread pool is straightforward, with a simple-minded work stealing policy.

Memory allocation occurs on-demand as a prelude of executing a node. All memory objects are immutable, which makes memory management simple. While this seems to imply expensive malloc overhead, this is not so in practice. Deep learning training algorithms typically produce data of regular size, and we implement our own freelist that allows reuse of memory objects. The only subtle issue is garbage collection. We manipulate reference count at the time the computation node is generated and when it is executed. A memory object whose reference count drops to zero returns to the freelist.

The availability of a new data may trigger an RPC call to send to another process. On the other end there is a blocked receive call that awaits the data, which may in turn trigger blocked computation. The dataflow abstraction allows maximum degree of computation and communication overlapping, without the need of explicit scheduling.

We implement polymorphism to give the flexibility of calling into different library routines. When running on a multi-core CPU, we typically uses Intel's MKL library to carry out matrix computation. MKL is highly efficient and is itself multi-threaded and runs on multi-core. The number of MKL thread per matrix computation can be specified. Typically, the correct configuration is when number of user thread (the size of the thread pool) times the MKL threads equals to number of cores. We will show that this additional flexibility buys us performance gains, sometimes substantially (see Section~\ref{sec:Evaluation}).

When GPU is available, the computation is carried out entirely inside GPU, using Nvidia's CUBLAS library~\cite{nvidia2008cublas}. In this case, we additionally allocate GPU memory and make sure that the state of model always live in GPU, and training data is on-demand fetched from CPU memory. In case GPU runs low on memory, some memory holding training data is freed according to the LRU policy. Since training data is accessed sequentially, in practice this works quite well.

Especially for convolution network, there is a great amount of parallelism but each computation is small. Thus, we use CUDA-provided streams~\cite{cuda} to asynchronously execute all of them in parallel. CUDA operations in the same stream execute in issue-order, while operations in different streams may run concurrently. Multiple streams are driven by multiple CPU threads. This results in 2$\sim$4 times performance gain. Multiple, concurrent CPU-managed streams are important because this allows almost complete overlapping of computation with data fetching. However, this gain is made possible only after we get around an issue in CUDA driver: while it is capable of issuing multiple asynchronous calls, dispatching the call is not multi-thread friendly. Therefore, we use one CPU thread to dispatch the calls from a GPU task queue, whereas other CPU threads simply inserts their calls into that queue.

Note that our design allows the flexibility of leveraging both CPU and GPU simultaneously, but practical experience leads us to believe that this is an uninteresting option.

\subsection{Handling convolution}	\label{sec:convolution_design}

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{resource/Convolution.pdf}
\caption{Parallel convolution and its dataflow representation.}
\label{fig:Convolution}
\end{figure}

Convolution is a case that needs special handling, since parallelism is handled differently. 
%Convolution results in local receptive field, but if this is the only thing we need to worry, the problem is a matter of supporting sparse matrix computation efficiently. In convolution network, parameters are shared. Thus, storing them as a sparse matrix wastes a great deal of space, as it is proportional to input data (e.g. 224x224) instead of the filter size (e.g. 11x11).
Parallelism in CNN proceeds by dividing up the input and output data into smaller patches. A set of filters local to a process convolve over its partition. The dataflow construction of a convolution operation considers boundary conditions, and fetches data belonging to different partitions (Figure~\ref{fig:Convolution}). 

The convolution operator ({\tt C} in Figure~\ref{fig:Convolution}) is not implemented as a sparse matrix multiplication. For 2D input, the data is flattened into a row vector. As such, a filter, typically spanning multiple rows of the original data, needs to fetch non-consecutive portions of the flattened vector. Computing a single neuron's output is therefore decomposed into many small matrix multiplication. Obviously, this is not efficient. We follow the same optimization as in~\cite{coates2013deep} by first forming larger matrix. The gain of multiplication outweighs the overhead of additional memory copy, and is especially effective for GPU. 

While this handles feed forward nicely, back propagation of gradients involves a reduction that synchronizes all the local sets of filters, possibly across machine boundary. 
%The operator {\tt ConvOutProduct} that handles this is more complicated, but is completely hidden from the user. The details are beyond the scope of this paper, and will be covered in an upcoming technical report. 
To the best of our knowledge, ours is the only distributed implementation of a complete convolution network. Existing approaches either perform local receptive field only~\cite{dean2012distbelief}\cite{le2011building}\cite{coates2013deep}, or convolve only within a partition~\cite{krizhevsky2012imagenet}.

\subsection{User-enabled optimization}	\label{sec:user-opt}

When we scale-up the training, the most important optimization is to hide various I/O latencies. These optimizations are often scenario and platform dependent. We demonstrate how Minerva achieve them with only a few lines of code change.

For example, when data volume is too large to fit in memory, loading data off from the disk should proceed in parallel with computation. In Minerva, this can be done by letting a temporary matrix load the next batch of data at the beginning of an iteration, and switching to it at the end of the iteration, as illustrated below. Training on this batch and loading of next batch can thus be executed in parallel since there is no dependency between them in the dataflow graph generated before hitting {\tt EvalAll()}.

\begin{lstlisting}
thisBatch = LoadBatches(..., inputfiles[1]);
for(int n = 1; n < numBatchFiles; ++n) {
	nextBatch = LoadBatches(..., inputfiles[n+1]);
	...; // Training on thisBatch
	EvalAll();
	thisBatch = nextBatch;
}
\end{lstlisting}

For ImageNet data set, one mini-batch is about 100MB and takes about 0.3s to load on a server with SSD disk. We are able to completely hide this latency, even when computation is very fast (about 1s with GPU, see Section~\ref{sec:Scale-up})

Another example is to hide network latency in model parallelism. The output of a layer is one logical matrix that is partitioned among multiple machines. This matrix has number of rows equal to the mini-batch size. Each process needs to send its portion of this matrix to all other servers, incurring bursty and expensive data shuffling. Although our runtime already performs partial computation as data arrives, this expense is still visible. We deal with this problem by simply carving up one mini-batch into smaller mini-mini-batch, enabling pipelining across layers:

\begin{lstlisting}
for(int i = 0; i < numMiniBatches; ++i) {
	for(int j = 0; j < numMiniMiniBatches; ++j) {
		// Calculate gradient on the j'th mini-mini-batch
		gradients[j] = ...;
	}
	for(int j = 0; j < numMiniMiniBatches; ++j) {	
		parameters -= rate * gradients[j];	// Gradient decent
	}
	EvalAll();
}
\end{lstlisting}

The choice of mini-mini-batch size is a trade-off: if it is too big, we cannot hide much latency, and when it's too small, matrix computation loses efficiency. For the scale-out experiment for speech-net, the difference can be as big as 30\%.

%Another example is to hide network latency in model parallelism. The output of a layer is one logical matrix that is partitioned among multiple machines. This matrix has number of rows equal to the mini-batch size. Each process needs to send its portion of this matrix to all other servers, incurring bursty and expensive data shuffling. Although our runtime already performs partial computation as data arrives, this expensive is still visible. We deal with this problem by simply carving up one mini-batch into smaller mini-mini-batch, enabling pipelining across layers (Fig-x). The choice of mini-mini-batch size is a tradeoff: if it is too big, we cannot hide much latency, and when it's too small, matrix computation loses efficiency. For the scale-out experiment for speech-net, the difference can be as big as 50\% (check!.)
